# Meetup Abril 2020

## Palestras

[Abertura](https://devopspbs.gitlab.io/meetups/meetup-2020-04-23/)

Presente e Futuro da Linguagem Egua - Julio Leite e Brennus Caio

> As apresentações estão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
